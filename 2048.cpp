#include<stdio.h>
#include<conio.h>
#include<stdlib.h>
#include<windows.h>              
int a[4][4],score=0;
char zj[256];  
void logall(int ptscreen);                              /*全局的二维数组，即2048游戏界面所控制的4*4格子*/ 
void left(char di);                      
void right(char di);
void up(char di);
void down(char di);
int full(void);
int cc(void);
void build(void);
void writelog(char* s);    
void gotoxy(int x, int y) //将光标调整到(x,y)的位置
{
    HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);
    COORD pos;
    pos.X = x;
    pos.Y = y;
    SetConsoleCursorPosition(handle, pos);
}
     
int main()
{
	int c=0,f=0,i,j;
	char zw=0,s1[256],s2[256];
	
	srand(1);
	for(i=0;i<4;i++)
		for(j=0;j<4;j++)
			a[i][j]=0;
	while(zw!='X'){
	    c=cc();
		f=full();
		if(f==0&&c==0)
			printf("game over!");
		else
		{
	    	{
		    	zw=getch();
		    	switch(zw)
		    	{
					case'A':left(zw);break;
					case'D':right(zw);break;
					case'W':up(zw);break;
					case'S':down(zw);break;
		    	}
	    	}
    	}
    	
    	writelog("before build");
    	logall(0);
    	
    	for(i=0;i<256;i++)zj[i]=0;
    	zj[0]=zw;
    	zj[1]='\n';
    	writelog(zj);
    	build();
    	writelog("after build");
    	logall(1);
    	gotoxy(0,0);
		    }
}
void left(char di)                /*向左的函数*/            
{                    
    int i,j,x=0,l[5]={0,0,0,0,0};          
		for(i=0;i<4;i++)                 
		{
			for(x=0;x<5;x++) l[x]=0;     
			for(j=0,x=0;j<4;j++){        /*把所有有值之间的空去掉，移向左边*/     
				if(a[i][j]!=0){           
					l[x]=a[i][j];        
					x++;                 
				}
			}
			//for(j=0;j<4;j++)
			//	printf("%d ",a[i][j]);
			//printf("\n");
			
			for(j=0;j<4;j++)	a[i][j]=0;
			if(l[0]!=l[1]&&l[1]!=l[2]&&l[2]!=l[3]){
				for(x=0,j=0;x<4;x++){
					a[i][j]=l[x];
					j++;
				}
			}
			else{
			    for(x=0,j=0;x<4;x++){  
				    if(l[x]!=0){
					    if(l[x]==l[x+1]){
							a[i][j]=l[x]+l[x+1];
							score=score+a[i][j];
							l[x+1]=0;
							j++;
					    }  
					    else{
							a[i][j]=l[x];
						 	j++;
					    }
			     	}
				    else
					   continue;     
			    }
		    }
		    //for(j=0;j<4;j++)
			//	printf("%d ",a[i][j]);
			//printf("\n");
		}
}
void right(char di)                     /*向右的函数*/ 
{                    
    int i,j,x=3,r[5]={0,0,0,0,0};          
		for(i=0;i<4;i++)               
		{
			for(x=0;x<5;x++) r[x]=0;
			for(j=3,x=3;j>=0;j--){         
				if(a[i][j]!=0){         
					r[x]=a[i][j];    
					x--;               
				}
			}
			for(j=3;j>=0;j--)	a[i][j]=0;
			if(r[0]!=r[1]&&r[1]!=r[2]&&r[2]!=r[3]){
				for(x=3,j=3;x>=0;x--){
					a[i][j]=r[x];
					j--;
				}
			}
			else{
			    for(x=3,j=3;x>=0;x--){  
				    if(r[x]!=0){
					   if(r[x]==r[x-1]){
						a[i][j]=r[x]+r[x-1];
						score=score+a[i][j];
						r[x-1]=0;
						j--;
					   }  
					    else{
						 a[i][j]=r[x];
						 j--;
					    }
			     	}
				    else
					   continue;     
			    }
		    }
		}
}
void down(char di)                /*向下的函数*/            
{                    
    int i,j,x=3,d[5]={0,0,0,0,0};          
		for(j=0;j<4;j++)                 
		{
			for(x=0;x<5;x++) d[x]=0;
			for(i=3,x=3;i>=0;i--){             
				if(a[i][j]!=0){           
					d[x]=a[i][j];        
					x--;                 
				}
			}
			for(i=3;i>=0;i--)	a[i][j]=0;
			if(d[0]!=d[1]&&d[1]!=d[2]&&d[2]!=d[3]){
				for(x=3,i=3;x>=0;x--){
					a[i][j]=d[x];
					i--;
				}
			}
			else{
			    for(x=3,i=3;x>=0;x--){  
				    if(d[x]!=0){
					   if(d[x]==d[x-1]){
						a[i][j]=d[x]+d[x-1];
						score=score+a[i][j];
						d[x-1]=0;
						i--;
					   }  
					    else{
						 a[i][j]=d[x];
						 i--;
					    }
			     	}
				    else
					   continue;     
			    }
		    }
		}
}
void up(char di)                /*向上的函数*/            
{                    
    int i,j,x=0,u[5]={0,0,0,0,0};          
		for(j=0;j<4;j++)                 
		{
			for(x=0;x<5;x++) u[x]=0;
			for(i=0,x=0;i<4;i++){             
				if(a[i][j]!=0){           
					u[x]=a[i][j];        
					x++;                 
				}
			}
			for(i=0;i<4;i++)	a[i][j]=0;
			if(u[0]!=u[1]&&u[1]!=u[2]&&u[2]!=u[3]){
				for(x=0,i=0;x<4;x++){
					a[i][j]=u[x];
					i++;
				}
			}
			else{
			    for(x=0,i=0;x<4;x++){  
				    if(u[x]!=0){
					   if(u[x]==u[x+1]){
						a[i][j]=u[x]+u[x+1];
						score=score+a[i][j];
						u[x+1]=0;
						i++;
					   }  
					    else{
						 a[i][j]=u[x];
						 i++;
					    }
			     	}
				    else
					   continue;     
			    }
		    }
		}
}
int full(void)
{
	int i,j,flag=0;
	for(i=0;i<4;i++)
	{
		for(j=0;j<4;j++)
		{
			if(a[i][j]==0)
			{
				flag++;
			}
		}
	}
	if(flag==0)
		return 0;                       /*满状态*/
    else
	    return 1;                       /*非满状态*/ 
}
int cc(void)
{
	int i,j,q[6][6],flag=0,g,h;
	for(i=0,g=1;i<4,g<5;i++,g++)
	{
		for(j=0,h=1;j<4,h<5;j++,h++)
			q[g][h]=a[i][j];
	}
	for(g=1;g<5;g++)
	{
		for(h=1;h<5;h++)
		{
			if(q[g][h]==q[g][h+1]||q[g][h]==q[g][h-1]||q[g][h]==q[g+1][h]||q[g][h]==q[g-1][h])
				flag++;
		}
	}
	if(flag==0)
	    return 0;                       /*没有可以重合的情况*/ 
	else
	    return 1;                       /*有能重合的情况*/ 
}
void build(void)
{
	int i,j;
	while(1)
	{
		i=rand()%4;
		j=rand()%4;
		if(a[i][j]==0)
			{
				a[i][j]=2;
				break;
			}
	}
}
void writelog(char* s)
{
	FILE *fp;
    fp=fopen("2048.log", "a"); /*建立一个文字文件只写*/ 
    fputs(s,fp); /*向所建文件写入一串字符*/ 
     
    fclose(fp);
}
void logall(int ptscreen)
{
	int i,j,k;
	char s1[256],s2[256];
	for(i=0;i<4;i++){
			itoa(-1,s2,16);
			for(j=0;j<4;j++){
				itoa(a[i][j],s1,10);
				strcat(s1," ");
				k=-1;
		    	if (ptscreen!=0)
				 
					//printf("a[%d][%d]=%d ",i,j,a[i][j]);
					//printf("%6d",a[i][j]);
				if(a[i][j]==0)
					printf("%6d",k);
				else
					printf("%6d",a[i][j]);
				
				strcat(s2,s1);
			}

			strcat(s2,"\n");
			writelog(s2);
		
			if (ptscreen!=0) {
				printf("\n");
			}
		}
		if (ptscreen!=0) 
			printf("%24d\n",score);

}
